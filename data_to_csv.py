import h5py
import csv 



with h5py.File("chickpea-ssm-snp (1).h5", "r") as f, h5py.File("weather_ssm (2).h5", "r") as g, open('data.csv', 'w') as data_writer, open('response.csv', 'w') as response_writer:
    
    datawrite = csv.writer(data_writer) 
    responsewrite = csv.writer(response_writer) 
    
    
    for i in range(len(f['doy'])):
        tmp = f['gr_covar'][i].tolist()
        
        for j in range(len(g['doy'])):
            if g['doy'][j] == f['doy'][i] and g['month'][j] == f['month'][i] and g['year'][j] == f['year'][i] and g['geo_id'][j] ==f['geo_id'][i]:

                for k in range(j - 5,j + 20):
                    tmp.append(g['dl'][k][0])
                    tmp.append(g['rain'][k][0])
                    tmp.append(g['srad'][k][0])
                    tmp.append(g['tmax'][k][0])
                    tmp.append(g['tmin'][k][0])
        datawrite.writerow(tmp)
        responsewrite.writerow([f['response'][i][0]])
